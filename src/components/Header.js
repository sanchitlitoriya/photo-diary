import React from "react";
import { Navbar, NavbarBrand } from "reactstrap";

export const Header = () => {
  return (
    <div>
      <Navbar color="dark">
    
      <NavbarBrand ><h1>Instamiligram</h1></NavbarBrand>
      </Navbar>
    </div>
  );
};

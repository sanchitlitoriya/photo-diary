import React from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";
import {DisplayImages} from "../pages/DisplayImages";
import {Home} from "../pages/home/Home";

export default function Routes() {
  const currentUser = useSelector((state) => state.currentUser);
  const privateRoute = () => {
    if (currentUser.email !== "") {
      return <DisplayImages />;
    } else return <Home />;
  };
  return (
    <div>
      <BrowserRouter>
        <Route exact path="/details">
          {privateRoute()}
        </Route>
        <Route exact path="/">
          <Home />
        </Route>
      </BrowserRouter>
    </div>
  );
}

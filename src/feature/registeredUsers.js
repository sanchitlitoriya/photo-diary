import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const registerSlice = createSlice({
  name: "registeredUsers",
  initialState: {
    users: {},
  },
  reducers: {
    register: (state, action) => {
      const { email, ...rest } = action.payload;
      state.users = { ...state.users, [email]: rest };
      return state;
      // state[email] = {  ...rest };
    },
    addImage: (state, action) => {
      state.users[action.payload.email].images = action.payload.images;
      // console.log(state);
      // return state;
    },
    updateLikes: (state, action) => {
      state.users[action.payload.email].images.forEach((image) => {
        if (image.url === action.payload.url) {
          image.likes += 1;
          // console.log(image.likes);
        }
      });
      console.log(state.users[action.payload.email].images);
    },
  },
});

// export const fetchImage = createAsyncThunk('registeredUsers/fetchImage', async () => {
//   const response = await fetch('https://picsum.photos/200');
//   return response.url;
// })

export const { register, addImage, updateLikes } = registerSlice.actions;
export default registerSlice.reducer;

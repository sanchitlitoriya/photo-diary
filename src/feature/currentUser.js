import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "currentUser",
  initialState: {
    email: "",
  },
  reducers: {
    login: (state, action) => {
      // console.log(action.payload);
      state.email = action.payload;
    },
    logout: (state) => {
      state.email = "";
    },
  },
});

export const { login, logout } = userSlice.actions;
export default userSlice.reducer;

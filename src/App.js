import "./App.css";
import { DisplayImages } from "./pages/DisplayImages";

function App() {
  return <div className="App">
    <label className="App-header">display images</label>
    <DisplayImages/>
  </div>;
}

export default App;

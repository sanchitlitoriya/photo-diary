import { configureStore } from "@reduxjs/toolkit";
import registeredUsers from "./feature/registeredUsers";

import currentUser from "./feature/currentUser";

const store = configureStore({
  reducer: { currentUser, registeredUsers },
});

export default store;

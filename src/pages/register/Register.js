import React, { useState } from "react";
import { Button, Input } from "reactstrap";
import { useDispatch } from "react-redux";
import { register } from "../../feature/registeredUsers";
// import { login } from "../../feature/currentUser";
// import { useHistory } from "react-router";

export default function Register(props) {
  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const dispatch = useDispatch();
  // const history = useHistory();

  const handleRegister = (e) => {
    dispatch(register({ name, age, email, password, images: [] }));
    // dispatch(login(email));
    props.cb('login');
    // history.push("/details");
  };
  return (
    <div>
      <Input
        onChange={(e) => setEmail(e.target.value)}
        type="email"
        placeholder="email"
        id="email"
      />
      <br />
      <br />
      <Input
        onChange={(e) => setName(e.target.value)}
        type="text"
        placeholder="Name"
        id="name"
      />
      <br />
      <br />
      <Input
        onChange={(e) => setAge(e.target.value)}
        type="number"
        placeholder="age"
        id="age"
      />
      <br />
      <br />
      <Input
        onChange={(e) => setPassword(e.target.value)}
        type="password"
        placeholder="password"
        id="password"
      />
      <br />
      <br />
      <Button outline onClick={handleRegister} color="success">
        Register
      </Button>
    </div>
  );
}

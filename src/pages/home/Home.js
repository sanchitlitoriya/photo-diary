import React, { useState } from "react";
import { Button, ButtonGroup } from "reactstrap";
import Login from "../login/Login";
import Register from "../register/Register";

export const Home = () => {
  const [toggleLoginRegister, setToggleLoginRegister] = useState("login");

  const renderComponent = () => {
    if (toggleLoginRegister === "login") {
      return <Login />;
    } else if (toggleLoginRegister === "register") {
      return <Register cb={setToggleLoginRegister}/>;
    }
  };
  const handleButtonClick = (e) => {
    setToggleLoginRegister(e.target.id);
  };
  return (
    <div style={{textAlign:'center',padding:'10%' }}>
      <ButtonGroup style={{ padding: "5%" }}>
        <Button
          id="login"
          outline
          color={toggleLoginRegister === "register" ? "secondary" : "primary"}
          onClick={(e) => handleButtonClick(e)}
        >
          Login
        </Button>
        <Button
          outline
          id="register"
          color={toggleLoginRegister === "register" ? "primary" : "secondary"}
          onClick={(e) => handleButtonClick(e)}
        >
          Register
        </Button>
      </ButtonGroup>
      <br />
      <br />
      {renderComponent()}
    </div>
  );
};

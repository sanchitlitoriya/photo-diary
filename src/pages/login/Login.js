import React, { useState } from "react";
import { Button, Input } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../feature/currentUser";
import { useHistory } from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();
  const registeredUsers = useSelector((state) => state.registeredUsers);
// console.log(registeredUsers);
  const handleLogin = () => {
    const userFound = registeredUsers.users[email];
    if (userFound) {
      // console.log(email, pass);
      if (userFound.password === pass) {
        // console.log(userFound);
        dispatch(login(email));
        history.push("/details");
      } else alert("invalid password");
    } else alert("email or password incorrect");
  };
  return (
    <div>
      <Input    
        onChange={(e) => setEmail(e.target.value)}
        placeholder="email"
        id="email"
        required={true}
      />
      <br />
      <br />
      <Input
        onChange={(e) => setPass(e.target.value)}
        type="password"
        placeholder="password"
        id="password"
        required={true}
      />
      <br />
      <br />
      <Button outline onClick={handleLogin} variant="contained" color="success">
        Login
      </Button>
    </div>
  );
}

import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner,
} from "reactstrap";
import { useSelector, useDispatch } from "react-redux";
import { addImage, updateLikes } from "../feature/registeredUsers";
import { logout } from "../feature/currentUser";
import { useHistory } from "react-router-dom";

export const DisplayImages = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.currentUser);
  const registeredUsers = useSelector((state) => state.registeredUsers.users);
  const [modal, setModal] = useState(false);
  const [showSpinner, setshowSpinner] = useState(false);
  const [modelData, setModelData] = useState({});
  const [images, setImages] = useState(
    registeredUsers[currentUser.email].images
  );

  const getImages = async () => {
    setshowSpinner(true);
    const response = await fetch("https://picsum.photos/200");
    const image = {
      url: response.url,
      likes: 0,
      description: "like this photo to show you support to photographer",
    };
    setImages((prevState) => [...prevState, image]);
  };

  useEffect(() => {
    dispatch(addImage({ email: currentUser.email, images: [...images] }));
    setshowSpinner(false);
  }, [images]);

  useEffect(() => {
    console.log(registeredUsers[currentUser.email].images);
  }, [registeredUsers])
  
  const handleLogout = () => {
    dispatch(logout());
    history.push("/");
  };

  const toggle = (image) => {
    setModelData({
      url: image.url,
      likes: image.likes,
      description: image.description,
    });
    setModal(!modal);
  };
  const likeUpdate = (image) => {
    // dispatch(
    //   updateLikes({
    //     email: currentUser.email,
    //     url: image.url,
    //     likes: image.likes,
    //   })
    // );
    setModal(!modal);
    const arrayImages = [...images];
    const newArray = arrayImages.map((i) => {
      const newData = {...i};
      if (i.url === image.url) {
        newData.likes += 1; // need help here
      }
      return newData;
    });
    console.log(newArray);
    setImages(newArray);
  };
  return (
    <div style={{ textAlign: "center" }}>
      <h1 style={{ color: "white" }}>
        Hello {registeredUsers[currentUser.email].name}
      </h1>
      <br />
      <Button
        outline
        disabled={showSpinner}
        color="primary"
        style={{ marginBottom: "20px" }}
        onClick={() => getImages()}
      >
        {showSpinner ? <Spinner type="border" /> : "Add image"}
      </Button>
      <br />
      {images.map((image, index) => {
        return (
          <img
            key={index}
            style={{ padding: "5px", cursor: "pointer" }}
            onClick={() => toggle(image)}
            alt={index}
            src={image.url}
          ></img>
        );
      })}
      <div>
        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>Image insights</ModalHeader>
          <ModalBody>
            <b>Description - </b> {modelData.description}
            <br />
            <b>Likes - </b>
            {modelData.likes}
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => likeUpdate(modelData)}>
              like
            </Button>{" "}
            <Button onClick={toggle}>close</Button>
          </ModalFooter>
        </Modal>
      </div>
      <label
        className="fixed-bottom "
        style={{ textAlign: "end", backgroundColor: "black", padding: "3px" }}
      >
        <Button onClick={handleLogout} color="danger">
          Logout
        </Button>
      </label>
    </div>
  );
};
